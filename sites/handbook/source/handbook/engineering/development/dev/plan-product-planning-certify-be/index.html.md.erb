---
layout: handbook-page-toc
title: Plan:Product Planning & Plan:Certify Backend Team
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Plan:Product Planning & Plan:Certify backend team

The Plan:Product Planning & Plan:Certify backend team works on the backend part of
GitLab's [Product Planning] and [Certify] categories in the [Plan stage].

For more details about the vision for this area of the product, see the
[Plan stage] page.

[Product Planning]: /handbook/product/categories/#product-planning-group
[Certify]: /handbook/product/categories/#certify-group
[Plan stage]: /direction/plan/

### Team members

<%= direct_team(manager_role: 'Backend Engineering Manager, Plan:Product Planning') %>

### Stable counterparts

<%= stable_counterparts(role_regexp: /[,&] (Plan(?!:)|Plan:Product Planning)/, direct_manager_role: 'Backend Engineering Manager, Plan:Product Planning') %>

### Hiring chart

This chart shows the progress we're making on hiring. Check out our
[jobs page](/jobs/) for current openings.

<%= hiring_chart(department: 'Plan:Product Planning BE Team') %>

### Team metrics dashboard 

<%= partial("handbook/engineering/development/dev/plan/product_planning_dashboard") %>

### Application performance dashboard

<%= partial("handbook/engineering/development/dev/plan/product_planning_application_dashboard") %>

## OKRs

<%= partial("handbook/engineering/development/dev/plan/product_planning_backend_okrs") %>

## Scalability Targets

### Stage-wide

<%= partial("handbook/engineering/development/dev/plan/scalability_targets") %>

### Product Planning & Certify

| Type | Description | Estimated Timeline for Failure | Resolution Due Date | 12 Month Target | Issue | Status |
| ---- | ---          | ---                            | ---                | ---    | ---   | ---    |
| Redis Primary CPU | `EmailReceiverWorker` creates expensive `lpush` and `hset` operations on `redis-sidekiq` primary  | September 2021 | September 2021 | [1200 Service Desk][servicedesk] issues per day | [gitlab-com/gl-infra&469](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/469) | <span style='border-radius:0.2em; font-weight:bold; padding-left:1em; padding-right:1em; color:white; background-color:red;'>Urgent</span> |

[servicedesk]: https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/11747#service-desk

## Work

See the [Plan stage page] and the [Plan:Project Management backend team page].

[Plan stage page]: /handbook/product/categories/plan/
[Plan:Project Management backend team page]: /handbook/engineering/development/dev/plan-project-management-be/

### Capacity Planning

<%= partial("handbook/engineering/development/dev/plan/capacity_planning") %>

#### Weighing bugs

<%= partial("handbook/engineering/development/dev/plan/weighing_bugs") %>

#### Planning Rotation

<%= partial("handbook/engineering/development/dev/plan/planning_rotation") %>

<%= partial("handbook/engineering/development/dev/plan/product_planning_planning_rotation") %>

#### Consider a Spike and/or a Design Document

Work that arrives in ~"workflow::ready for development" that is out of scope 
or ill-defined should be 
[returned to ~"workflow::planning breakdown" for further refinement][1].
To avoid the disruption this introduces we try to reduce the number of times 
it happens by planning more carefully. While it's not always possible, we aim 
to identify complexity before the build phase. For this reason, we assign
a backend [DRI][dri] to help with each upcoming deliverable during design and
validation phases.

However, sometimes complexity can't be accurately estimated until development 
work starts. If you anticipate this during planning, consider creating a spike to produce a
design document. Notify the participants in the issue, especially the PM, that 
a spike is required, create a separate issue and follow these
steps:

1. Title the issue with the goal of the spike;
1. Add the ~spike, ~backend, and corresponding stage/group labels;
1. List the unknowns and questions to be answered;
1. List decisions, architectural or otherwise, that need to be made;
1. Identify counterparts you'll need help from (e.g. Frontend or UX) and establish a DRI;
1. Mark the issue as blocking the original, and
1. Label with ~"workflow::ready for development" and assign to the current 
milestone for someone to pick up.

The deliverable is a design document that answers the questions set out in the
issue description. This can simply be the issue itself, containing a summary
of the discussion in the description, answers to the questions and links to
any PoC MRs produced. 


[1]: https://about.gitlab.com/handbook/product-development-flow/#build-phase-2-develop--test
[dri]: https://about.gitlab.com/handbook/people-group/directly-responsible-individuals/

#### Historical Capacity

<%= partial("handbook/engineering/development/dev/plan/historical_capacity", locals: { chart_ids: [7693825] }) %>

### Collaborating with Frontend

#### Workflow Labels

Most issues, especially features, involve working with other disciplines. A 
single issue will often be shared between frontend and backend and it
can be difficult to know which workflow label should be applied, especially
when progress is at different stages.

To ensure visibility for other team-members, for issues with a frontend and
backend component:
- Assign yourself as early as possible if you have capacity to work on it;
- Move the issue to the top of ~"workflow::ready for development" until a frontend engineer is assigned;
- When both are assigned, consider hosting a small kickoff discussion.
- When the backend work is merged and verified add [the ~"backend complete" label](#using-the-backend-complete-label).

We value [velocity over predictability](/handbook/engineering/#velocity-over-predictability)
so use your own judgement on whether you should wait for a frontend engineer to
get involved before proceeding with development.

#### Using the ~"backend complete" label

The ~"backend complete" label is added to issues with multiple specializations (usually backend and 
frontend) to indicate that the backend component is complete. Add this label when the backend work is 
functionally complete, merged and verified but frontend, or other, work is ongoing.

#### Documentation

Documentation should accompany code for any [new or changed functionality] as per our 
[definition of done]. This can become tricky when collaborating on a feature that is 
behind a feature flag.

Since all feature flags start as disabled by default, we should aim to document the
feature as soon as it's safe for testing by users using the [feature flag template].
Don't wait until a feature is performant and stable to document it, instead do so once
it's secure and won't leave data in a corrupt, interim state. 

Try to include docs with the first MR to introduce usable functionality. If this is 
an API addition with no UI, document that and allow the FE engineers to update it as 
work proceeds. As the feature flag rollout proceeds, the [documentation should be updated].

This avoids the rush to provide documentation that often accompanies the release cutoff.

[new or changed functionality]: https://about.gitlab.com/handbook/engineering/ux/technical-writing/workflow/#for-a-product-change
[definition of done]: https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html#definition-of-done
[feature flag template]: https://docs.gitlab.com/ee/development/documentation/feature_flags.html#features-disabled-by-default
[documentation should be updated]: https://docs.gitlab.com/ee/development/documentation/feature_flags.html#features-that-became-enabled-by-default

### Picking something to work on

The team [Build Board] always shows work in the current
release, with [workflow columns] relevant to implementation. Filtering it by
~backend shows issues for backend engineers to work on.

It's OK to not take the top item if you are not confident you can solve
it, but please post in [#s_plan] if that's the case, as this probably
means the issue should be better specified.

[workflow columns]: /handbook/product-development-flow/
[Build Board]: https://gitlab.com/groups/gitlab-org/-/boards/1569369?not[milestone_title]=Backlog
[#s_plan]: https://gitlab.slack.com/archives/s_plan

#### High Severity Issues

<%= partial("handbook/engineering/development/dev/plan/high_severity_items") %>

### Working on unscheduled issues

Everyone at GitLab has the freedom to manage their work as they see fit,
because [we measure results, not hours][results]. Part of this is the
opportunity to work on items that aren't scheduled as part of the
regular monthly release. This is mostly a reiteration of items elsewhere
in the handbook, and it is here to make those explicit:

1. We expect people to be [managers of one][efficiency], and we [use
   GitLab ourselves][collaboration]. If you see something that you think
   is important, you can [request for it to be scheduled], or you can
   [work on a proposal yourself][iteration], as long as you keep your
   other tasks in mind.
2. From time to time, there are events that GitLab team-members can participate
   in, like the [issue bash]. Anyone is welcome
   to participate in these.
3. If you feel like you want to have some specific time set aside, but
   aren't interested in the topics of an existing event, feel free to
   label issues with "For Scheduling" and copy your manager for visibility.

When you pick something to work on, please:

1. Follow the standard workflow and assign it to yourself.
2. Share it in [#s_plan] - if not even more widely (like in #development
   or #backend).

[collaboration]: /handbook/values/#collaboration
[results]: /handbook/values/#results
[efficiency]: /handbook/values/#efficiency
[iteration]: /handbook/values/#iteration

[request for it to be scheduled]: /handbook/engineering/workflow/#requesting-something-to-be-scheduled
[issue bash]: /community/issue-bash/

## Useful links

<%= partial("handbook/engineering/development/dev/plan/useful_links", locals: { board: { name: 'Plan:Product Planning', url: 'https://gitlab.com/groups/gitlab-org/-/boards/1569369?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Aplan&not[label_name][]=group%3A%3Aproject%20management' }}) %>

